from scipy.special import gamma, factorial

G = 9.81

def U10(wp):
    return 0.855*G/wp

def ufunc(w,wp):
    if w>wp:
        return -2.5
    return 5.0

def sfunc(w,wp):
    #return 11.5*(G/(wp*U10(wp)))**2.5 * (w/wp)**ufunc(w,wp)
    return 17.01*(w/wp)**ufunc(w,wp)

def Nfunc(s):
    return 1.0/(2.0*sqrt(pi)) * gamma(s+1)/gamma(s+0.5)

def Dfunc(w,theta,wp):
    s = sfunc(w,wp)
    return Nfunc(s)*cos(theta/2.0)**(2.0*s)


Tp = 10  # Doesn't matter
wp = 2*pi/Tp

thetas = linspace(-pi,pi,100)

figure(10)
clf()



wfactors = [0.5, 0.75, 1.0, 1.5, 2.0]
wfactors = linspace(0.1,10,100)
mu2 = []
for wfactor in wfactors:
    w = wp*wfactor

    D = []
    for theta in thetas:
        D.append(Dfunc(w,theta,wp))

    D = array(D)
    # Moments
    mu=[]
    mu.append(trapz(D,thetas))
    mu.append(trapz(thetas*D,thetas))
    mu.append(trapz(thetas**2*D,thetas))
    print mu
    mu2.append(mu[-1])
    
    figure(10)
    #plot(thetas,D,label=r'$\omeg+a=(%0.1f)\omega_p$'%wfactor)
    plot(thetas,D,label=r'$\bar{\omega}=%0.2f$'%wfactor)
    #plot(thetas,D,label=r'$T=T_p/%0.1f$'%wfactor)

    
figure(10)
grid(True)
unit=0.5
ax = gca()
x_tick = arange(-1,1+unit,unit)
x_label = [r'$-\pi$',
           r'$-\pi/2$',
           r'$0$',
           r'$\pi/2$',
           r'$\pi$']
ax.set_xticks(x_tick*pi)
ax.set_xticklabels(x_label)
legend()
xlim([-pi,pi])
xlabel(r'$\Delta\theta$ [radians]')
ylabel(r'Directional Spreading Function $D(\bar{\omega},\Delta\theta)$ [n/a]')


figure(11)
clf()
plot(wfactors,sqrt(array(mu2)))
grid(True)
xlabel(r'$\bar{\omega} = \omega/\omega_p$ [n/a]')
ylabel('Square Root of Second Moment [rad]')

y_tick = array([1.0/8, 0.25, 3.0/8, 0.5])
y_label = [r'$\pi/8$',
           r'$\pi/4$',
           r'$3\pi/8$',
           r'$\pi/2$']
ax = gca()
ax.set_yticks(y_tick*pi)
ax.set_yticklabels(y_label)
#gca().invert_yaxis()

figure(12)
clf()
plot(wfactors,180.0/pi*sqrt(array(mu2)))
grid(True)
#gca().invert_yaxis()
xlabel(r'$\bar{\omega} = \omega/\omega_p$ [n/a]')
ylabel('Square Root of Second Moment [deg]')
show()
