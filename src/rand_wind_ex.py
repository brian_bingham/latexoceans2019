# calculate current variable wind velocity
#  double currentVarVel = this->previousVarVel + (-1/this->timeConstant*
#    (this->previousVarVel+this->gainConstant*randomDist))*dT;

figure(1)
clf()

sigs = [1, 3, 1]
taus = [1, 1 , 10]

# Time 
t0 = 60  # 100*tau
dt = 0.1
tt = arange(0,t0,dt)

# Gaussian samples
ww = randn(len(tt))

for sig,tau,ii in zip(sigs,taus,range(len(sigs))):
    # Standard deviation
    #sig = 10
    # Time constant
    #tau = 1.0
    #dt = 0.001 #tau/10.0

    K = sig*sqrt(2*tau/dt)


    vv = []
    vv.append(0.0)
    for t,w,i in zip(tt[1:],ww[1:],range(1,len(tt))):
      v = vv[-1] + (1.0/tau)*(-vv[-1]+K*w)*dt
      vv.append(v)

    # Veify
    print("Stdev, specified %.3f, stdev, measured %.3f"%(sig,std(vv)))
    #print("Gain = "+str(Gain)+", K = "+str(K)+", std = %.2f, K/sqrt(2tau) = %0.2f"%(std(vv),K/(sqrt(2*tau))))
    figure(1)
    #lstr = (r'$\sigma_v=%.1f$, $\tau_v=%.1f$, $\delta t = %.3f$'%(sig,tau,dt))
    lstr = (r'$\sigma_v=%.1f$, $\tau_v=%.1f$'%(sig,tau))
    lw = 1
    if ii == 0:
        lw = 2
    plot(tt,vv,label=lstr,linewidth=lw)

    
xlabel('Time [s]')
ylabel(r'Variable Wind Speed ($v(t)$) [m/s]')
grid(True)
legend()
ylim([-10,10])
show()



