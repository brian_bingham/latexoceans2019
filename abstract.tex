\documentclass[11pt,final,journal,onecolumn]{../latexlib/ieee/IEEEtran}

\usepackage{helvet}
\usepackage{cprotect}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{indentfirst}
\usepackage{float}
\usepackage{pdfpages}
\usepackage{hyperref}
\usepackage{todonotes}
\usepackage{subcaption}
\usepackage{fullpage}
\usepackage[none]{hyphenat}

% Prevent hyphenating.
\tolerance=1
\emergencystretch=\maxdimen
\hyphenpenalty=10000
\hbadness=10000

\begin{document}

% paper title
%\title{Towards an Experimentally Validated Plume Model to Support Robotic Plume Characterization}
\title{Toward Maritime Robotic Simulation in Gazebo}

% author names and affiliations
% use a multiple column layout for up to three different
% affiliations
\author{Brian~Bingham$^{1}$, Carlos Ag{\"u}ero$^{2}$, Michael McCarrin$^{1}$, Joseph Klamo$^{1}$, Joshua Malia and Kevin Allen$^3$ % <-this % stops a space
  \thanks{
    $^{1}$ Naval Postgraduate School, Monterey, CA, USA. {\tt\small <bbingham@nps.edu>}
    \newline \indent
    $^{2}$ Open Robotics, Mountain View, CA, USA {\tt\small <caguero@openrobotics.org>}%
    \newline \indent
    $^{2}$ University of Florida, Gainesville, FL, USA {\tt\small <kmallen@ufl.edu>}}%
}

% make the title area
\maketitle
% Remove the page number next to the title.
\thispagestyle{empty}

%\IEEEpeerreviewmaketitle

\section{Abstract}

Simulation is a fundamental capability for developing robotic applications.  By providing an approximation of complex scenarios and environments, simulators increase the speed of development while lowering the cost.  For much of the robotics community the open source Gazebo robot simulator\footnote{\url{http://gazebosim.org}} has emerged as the \emph{de facto} standard environment for prototyping and testing robotic systems.  As a result of this coalescence, the capabilities of the simulator have increased dramatically as academic, industry and government users contribute reusable, well-documented improvements and additions.  However, while terrestrial, aerial and space robotics applications are well supported, there is only limited support for maritime applications involving vehicles at and below the water surface.


Developing maritime robotic applications requires representing environmental and dynamic characteristics unique to the maritime environment, and these representations are not currently well supported in Gazebo. While a number of successful projects have addressed portions of this challenge, particularly for fully underwater applications, there is a need for new capabilities to enable simulation of robotic platforms working concurrently below, at and above the water surface.  We present current progress towards developing this capability in the context of the new 2019 virtual RobotX  (VRX) maritime robotics competition.


We identify a capability gap in existing simulation environments pertinent to supporting rapid development of maritime robotic solutions.  While a standard simulator for maritime robotics is not yet available, a number of previous efforts have recognized the need for simulation as part of the development process and highlighted the unique challenges of maritime simulations \cite{prats12open,tosik16mars,henriksen16morse,watanabe15rock,melman15distributed}.  Some of these simulations build upon the foundation of Gazebo \cite{demarco15computationally,manhaes16uuv} and others use different underlying simulation frameworks to emulate the dynamics.  An overview of the current technology is provide by \cite{cook14survey}.  Using the Maritime RobotX Challenge \footnote{\url{https://www.robotx.org/}} as an example, we identify opportunities for extending Gazebo to support some of the unique environmental (e.g., ocean waves), dynamic (e.g., hydrodynamics at the air-sea interface) and sensory (e.g., acoustic ranging) aspects of this application of robotics.

  Based on these identified capability gaps, we present recent progress in developing models and plugins to extend Gazebo, including demonstration of the existing capabilities from the VRX open-source repository\footnote{\url{https://bitbucket.org/osrf/vrx/}}.  We highlight some of the more general-purpose aspects of this work including the following plugins and models:
\begin{itemize}
\item Ocean wave field generation, visual rendering and surface vessel dynamic physical response,
\item Wind representation and vessel dynamic response,
\item Surface vessel hydrodynamic model based on empirical naval architecture coefficients,
\item Non-linear vessel propulsion model and
\item Visual, collision and physical model of the WAM-V unmanned surface vessel \footnote{\url{http://www.wam-v.com/}}.
\end{itemize}

The long term goal of this project is to bring together existing tools from the greater robotics community, while adding the domain-specific capabilities necessary to leverage these tools to support maritime robotic solution development.  This new simulation environment is capable of representing robotic surface vessels with authentic reproductions of environmental conditions and sensor data to support development of perception, navigation and autonomy solutions.  The result is a general purpose robotic simulation toolkit that will be immediately useful to maritime robotic projects in particular and multi-domain robotics projects in general.


\bibliographystyle{../latexlib/ieee/IEEEtran}
\bibliography{../latexlib/bib/robotx}

\end{document}


