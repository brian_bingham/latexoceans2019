# latexoceans2019

For use with [latexlib](https://bitbucket.org/brian_bingham/latexlib)

# Quickstart

 1. Make a workspace: `mkdir ~/latex_ws`
 2. Clone this and the library: `cd ~/latex_ws && git clone git@bitbucket.org:brian_bingham/latexlib.git  && git clone git@bitbucket.org:brian_bingham/latexoceans2019.git`
 3. Make: `cd ~/latex_ws/latexoceans2019 && latexmk -pdf toward_maritime_sim.tex`